import {
    NodeBlock,
    Slots,
    assert,
    castToNumber,
    findFirst,
} from "@tripetto/runner";

import "./conditions/no";
import "./conditions/yes";
import "./conditions/score";

export interface IYesNo {
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
    readonly altYes?: string;
    readonly altNo?: string;
    readonly aliasYes?: string;
    readonly aliasNo?: string;
    readonly colorYes?: string;
    readonly colorNo?: string;
    readonly scoreForTrue?: number;
    readonly scoreForFalse?: number;
}

export abstract class YesNo extends NodeBlock<IYesNo> {
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    readonly answerSlot = assert(
        this.valueOf<string, Slots.String>("answer", "static", {
            modifier: (data) => {
                let reference = data.reference;

                if (reference !== "yes" && reference !== "no") {
                    if (data.string) {
                        const yes = [
                            this.props.aliasYes || "",
                            this.parseVariables(this.props.altYes || ""),
                            "Yes",
                        ];
                        const no = [
                            this.props.aliasNo || "",
                            this.parseVariables(this.props.altNo || ""),
                            "No",
                        ];

                        if (findFirst(yes, (s) => s === data.string)) {
                            reference = "yes";
                        } else if (findFirst(no, (s) => s === data.string)) {
                            reference = "no";
                        } else if (
                            findFirst(
                                yes,
                                (s) =>
                                    s.toLowerCase() ===
                                    data.string.toLowerCase()
                            )
                        ) {
                            reference = "yes";
                        } else if (
                            findFirst(
                                no,
                                (s) =>
                                    s.toLowerCase() ===
                                    data.string.toLowerCase()
                            )
                        ) {
                            reference = "no";
                        }
                    } else {
                        reference = undefined;
                    }
                }

                return {
                    value:
                        reference === "yes"
                            ? this.props.aliasYes ||
                              this.parseVariables(this.props.altYes || "") ||
                              "Yes"
                            : reference === "no"
                            ? this.props.aliasNo ||
                              this.parseVariables(this.props.altNo || "") ||
                              "No"
                            : undefined,
                    reference: reference,
                    display: data.display,
                };
            },
            onChange: (answer) => {
                if (this.scoreSlot) {
                    this.scoreSlot.set(
                        answer.hasValue
                            ? castToNumber(
                                  answer.reference === "yes"
                                      ? this.props.scoreForTrue
                                      : this.props.scoreForFalse
                              )
                            : undefined
                    );
                }
            },
        })
    );

    readonly required = this.answerSlot.slot.required || false;

    toggle(data: "yes" | "no"): void {
        const sReference =
            this.answerSlot.reference === data ? undefined : data;

        this.answerSlot.set("", sReference);
    }
}

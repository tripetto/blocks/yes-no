/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: `${PACKAGE_NAME}:no`,
    alias: "yes-no:no",
})
export class No extends ConditionBlock {
    @condition
    isNo(): boolean {
        const answerSlot = this.valueOf<string>();

        return answerSlot ? answerSlot.reference === "no" : false;
    }
}

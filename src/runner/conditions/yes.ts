/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: `${PACKAGE_NAME}:yes`,
    alias: "yes-no:yes",
})
export class Yes extends ConditionBlock {
    @condition
    isYes(): boolean {
        const answerSlot = this.valueOf<string>();

        return answerSlot ? answerSlot.reference === "yes" : false;
    }
}

/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Markdown,
    markdownifyToString,
    pgettext,
    tripetto,
} from "@tripetto/builder";
import { YesNo } from "..";

/** Assets */
import ICON from "../../../assets/no.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:no`,
    version: PACKAGE_VERSION,
    alias: "yes-no:no",
    icon: ICON,
    get label() {
        return pgettext("block:yes-no", "No");
    },
})
export class No extends ConditionBlock {
    get name() {
        return (
            (this.node?.block instanceof YesNo ? this.node.block.altNo : "") ||
            this.type.label
        );
    }

    get title() {
        return markdownifyToString(this.name, Markdown.MarkdownFeatures.None);
    }
}

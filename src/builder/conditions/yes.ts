/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Markdown,
    markdownifyToString,
    pgettext,
    tripetto,
} from "@tripetto/builder";
import { YesNo } from "..";

/** Assets */
import ICON from "../../../assets/yes.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:yes`,
    version: PACKAGE_VERSION,
    icon: ICON,
    alias: "yes-no:yes",
    get label() {
        return pgettext("block:yes-no", "Yes");
    },
})
export class Yes extends ConditionBlock {
    get name() {
        return (
            (this.node?.block instanceof YesNo ? this.node.block.altYes : "") ||
            this.type.label
        );
    }

    get title() {
        return markdownifyToString(this.name, Markdown.MarkdownFeatures.None);
    }
}

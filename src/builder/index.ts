/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Forms,
    Markdown,
    NodeBlock,
    REGEX_IS_URL,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    insertVariable,
    isString,
    markdownifyToString,
    metadata,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { ICalculatorMetadata } from "@tripetto/block-calculator";
import { IYesNo } from "../runner";
import { Yes } from "./conditions/yes";
import { No } from "./conditions/no";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    alias: "yes-no",
    get label() {
        return pgettext("block:yes-no", "Yes/No");
    },
})
export class YesNo extends NodeBlock implements IYesNo {
    public answerSlot!: Slots.String;

    @definition("string", "optional")
    imageURL?: string;

    @definition("string", "optional")
    imageWidth?: string;

    @definition("boolean", "optional")
    imageAboveText?: boolean;

    @definition("string", "optional")
    @affects("#name")
    altYes?: string;

    @definition("string", "optional")
    @affects("#name")
    altNo?: string;

    @definition("string", "optional")
    aliasYes?: string;

    @definition("string", "optional")
    aliasNo?: string;

    @definition("number", "optional")
    scoreForTrue?: number;

    @definition("number", "optional")
    scoreForFalse?: number;

    @definition("string", "optional")
    colorYes?: string;

    @definition("string", "optional")
    colorNo?: string;

    @metadata("calculator")
    get calculator(): ICalculatorMetadata {
        return {
            answer: {
                allowDefault: false,
                allowCastToNumber: false,
                scores: [
                    {
                        reference: "yes",
                        label:
                            markdownifyToString(
                                this.altYes || "",
                                Markdown.MarkdownFeatures.None
                            ) || pgettext("block:yes-no", "Yes"),
                        score: this.scoreForTrue,
                    },
                    {
                        reference: "no",
                        label:
                            markdownifyToString(
                                this.altNo || "",
                                Markdown.MarkdownFeatures.None
                            ) || pgettext("block:yes-no", "No"),
                        score: this.scoreForFalse,
                    },
                ],
            },
        };
    }

    @slots
    defineSlot(): void {
        this.answerSlot = this.slots.static({
            type: Slots.String,
            reference: "answer",
            label: pgettext("block:yes-no", "Answer"),
            exchange: ["required", "alias", "exportable"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name(true, true);
        this.editor.description();
        this.editor.option({
            name: pgettext("block:yes-no", "Image"),
            form: {
                title: pgettext("block:yes-no", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(pgettext("block:yes-no", "Image source URL"))
                        .inputMode("url")
                        .placeholder("https://")
                        .action("@", insertVariable(this))
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 26 &&
                                      ref.value.indexOf(
                                          "data:image/svg+xml;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 19 &&
                                      ref.value.indexOf(
                                          "data:image/svg+xml,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext("block:yes-no", "Image width (optional)")
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:yes-no",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });
        this.editor.explanation();

        const defaultLabelForYes = pgettext("block:yes-no", "Yes");
        const defaultLabelForNo = pgettext("block:yes-no", "No");
        const sLabelForYes =
            markdownifyToString(
                this.altYes || "",
                Markdown.MarkdownFeatures.None
            ) || defaultLabelForYes;
        const sLabelForNo =
            markdownifyToString(
                this.altNo || "",
                Markdown.MarkdownFeatures.None
            ) || defaultLabelForNo;
        const scoreForYes = new Forms.Numeric(
            Forms.Numeric.bind(this, "scoreForTrue", undefined)
        ).label(sLabelForYes);
        const scoreForNo = new Forms.Numeric(
            Forms.Numeric.bind(this, "scoreForFalse", undefined)
        ).label(sLabelForNo);
        const aliasForYes = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "aliasYes", undefined, "")
        )
            .placeholder(sLabelForYes)
            .visible(isString(this.aliasYes))
            .indent(32);
        const aliasForNo = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "aliasNo", undefined, "")
        )
            .placeholder(sLabelForNo)
            .visible(isString(this.aliasNo))
            .indent(32);
        const aliasForYesCheckbox = new Forms.Checkbox(
            sLabelForYes,
            isString(this.aliasYes)
        ).on((alias) => {
            aliasForYes.visible(alias.isChecked);
        });
        const aliasForNoCheckbox = new Forms.Checkbox(
            sLabelForNo,
            isString(this.aliasNo)
        ).on((alias) => {
            aliasForNo.visible(alias.isChecked);
        });
        const colorForYes = new Forms.ColorPicker(
            Forms.ColorPicker.bind(this, "colorYes", undefined)
        )
            .label(sLabelForYes)
            .placeholder(
                pgettext(
                    "block:yes-no",
                    "Select an alternative color for this button"
                )
            )
            .swatches(false, true);
        const colorForNo = new Forms.ColorPicker(
            Forms.ColorPicker.bind(this, "colorNo", undefined)
        )
            .label(sLabelForNo)
            .placeholder(
                pgettext(
                    "block:yes-no",
                    "Select an alternative color for this button"
                )
            )
            .swatches(false, true);

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:yes-no", "Labels"),
            form: {
                title: pgettext("block:yes-no", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "altYes", undefined)
                    )
                        .placeholder(pgettext("block:yes-no", "Yes"))
                        .action("@", insertVariable(this, "exclude"))
                        .on((labelForYes) => {
                            const label =
                                (labelForYes.isFeatureEnabled &&
                                    markdownifyToString(
                                        labelForYes.value,
                                        Markdown.MarkdownFeatures.None
                                    )) ||
                                defaultLabelForYes;

                            colorForYes.label(label);
                            scoreForYes.label(label);
                            aliasForYesCheckbox.label(label);
                            aliasForYes.placeholder(label);
                        }),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "altNo", undefined)
                    )
                        .placeholder(pgettext("block:yes-no", "No"))
                        .action("@", insertVariable(this, "exclude"))
                        .on((labelForNo) => {
                            const label =
                                (labelForNo.isFeatureEnabled &&
                                    markdownifyToString(
                                        labelForNo.value,
                                        Markdown.MarkdownFeatures.None
                                    )) ||
                                defaultLabelForNo;
                            colorForNo.label(label);
                            scoreForNo.label(label);
                            aliasForNoCheckbox.label(label);
                            aliasForNo.placeholder(label);
                        }),
                ],
            },
            activated: isString(this.altYes) || isString(this.altNo),
        });
        this.editor.option({
            name: pgettext("block:yes-no", "Colors"),
            form: {
                title: pgettext("block:yes-no", "Colors"),
                controls: [colorForYes, colorForNo],
            },
            activated: isString(this.colorYes) || isString(this.colorNo),
        });

        this.editor.groups.options();
        this.editor.required(this.answerSlot);
        this.editor.visibility();

        this.editor.scores({
            target: this,
            scores: [scoreForYes, scoreForNo],
        });

        this.editor.option({
            name: pgettext("block:yes-no", "Alias"),
            form: {
                title: pgettext("block:yes-no", "Alias"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this.answerSlot, "alias", undefined)
                    ).placeholder(pgettext("block:yes-no", "Identifier")),
                    aliasForYesCheckbox,
                    aliasForYes,
                    aliasForNoCheckbox,
                    aliasForNo,
                    new Forms.Static(
                        pgettext(
                            "block:yes-no",
                            "These aliases will be used as identifiers for the item and its answers in the dataset."
                        )
                    ),
                ],
            },
            activated: isString(this.answerSlot.alias),
        });

        this.editor.exportable(this.answerSlot);
    }

    @conditions
    defineConditions(): void {
        this.conditions.template({
            condition: Yes,
            label: this.altYes || Yes.label,
            burst: true,
            props: {
                slot: this.answerSlot,
            },
        });

        this.conditions.template({
            condition: No,
            label: this.altNo || No.label,
            burst: true,
            props: {
                slot: this.answerSlot,
            },
        });

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(
                score.label,
                ICON_SCORE,
                false,
                true
            );

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext("block:yes-no", "Score is equal to"),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:yes-no",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext("block:yes-no", "Score is lower than"),
                    },
                    {
                        mode: "above",
                        label: pgettext("block:yes-no", "Score is higher than"),
                    },
                    {
                        mode: "between",
                        label: pgettext("block:yes-no", "Score is between"),
                    },
                    {
                        mode: "not-between",
                        label: pgettext("block:yes-no", "Score is not between"),
                    },
                    {
                        mode: "defined",
                        label: pgettext("block:yes-no", "Score is calculated"),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:yes-no",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
